# almundo-call-center

Repositorio prueba tecnica almundo el cual simula la funcionalidad de un call center

## Iniciando

Estas instrucciones le permitirán obtener una copia del proyecto y su respectiva configuracion para montaje del entorno de desarrollo

### Prerrequisitos

Requerimientos de software

```
* Git
* Java JDK 1.8
* Eclipse Neon o superior
* Gradle
* Maven
```

#Frameworks utilizados

```
* Sprint Boot
```

### Instalando

Paso a paso configuración entorno

##### Dirigirse al directorio DOCUMENTACION y seguir los pasos de las guias de configuración deacuerdo al proyecto que quiera configurar (Gradle o Maven)

* /DOCUMENACION/GUIA CONFIGURACION AMBIENTE GRADLE.pdf
* /DOCUMENTACION/GUIA CONFIGURACION AMBIENTE MAVEN.pdf

#### Requerimiento de la solución

* /DOCUMENTACION/Ejercicio Java.pdf

#### Change log ###
	Version 0.1: Implementación simulación call center almundo