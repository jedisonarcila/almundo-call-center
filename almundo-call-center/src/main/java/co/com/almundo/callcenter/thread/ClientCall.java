package co.com.almundo.callcenter.thread;

import java.util.concurrent.BlockingQueue;

import co.com.almundo.callcenter.model.Call;

public class ClientCall implements Runnable {

	private BlockingQueue<Call> queue;
	private Call call;
	
	public ClientCall(BlockingQueue<Call> queue, Call call) {
		this.queue = queue;
		this.call = call;
	}	
	
	public void run() {		
		try {
			queue.put(call);
			System.out.println("Encolando call - " + call);				
		} catch (InterruptedException e) {			
			e.printStackTrace();
		}		
	}
}
