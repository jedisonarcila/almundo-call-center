package co.com.almundo.callcenter.service;

import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.com.almundo.callcenter.model.Call;
import co.com.almundo.callcenter.model.Employee;
import co.com.almundo.callcenter.thread.ClientCall;
import co.com.almundo.callcenter.thread.DispatcherCall;
import co.com.almundo.callcenter.utils.Mocks;

@Service("callService")
public class CallService {

	@Autowired
	Mocks mocks;
	
	/**
	 * Method start the process calls
	 * 
	 * @param calls
	 */
	public boolean startCall(List<Call> calls) {
		try {
			List<Employee> employee = mocks.getEmployee();
			BlockingQueue<Call> queue = new ArrayBlockingQueue<Call>(10);
			fifoCall(calls, queue);
			DispatcherCall server = new DispatcherCall(queue, employee);
			new Thread(server).start();
			Thread.sleep(30000);
			System.out.println("Fin de la ejecución ...");
			return true;		
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}		
	}
	
	/**
	 * Method to keep the call, it used the concept, first-in first-out  
	 * 
	 * @param calls
	 * @param queue
	 */
	public boolean fifoCall(List<Call> calls, BlockingQueue<Call> queue) {
		for (Call call : calls) {
			ClientCall client = new ClientCall(queue, call);
			new Thread(client).start();
		}		
		return true;
	}
}
