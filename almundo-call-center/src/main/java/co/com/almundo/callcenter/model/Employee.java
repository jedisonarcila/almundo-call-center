package co.com.almundo.callcenter.model;

/**
 * Class to represent to a employee object
 * 
 * @author jarcila
 *
 */
public class Employee implements Comparable<Employee>{
	
	private String name;
	private String id;
	private String typeEmployee;
	private String status;
	private int priority;	
	
	/**
	 * @return the nombre
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param nombre the nombre to set
	 */
	public void setName(String nombre) {
		this.name = nombre;
	}
	/**
	 * @return the identificacion
	 */
	public String getId() {
		return id;
	}
	/**
	 * @param identificacion the identificacion to set
	 */
	public void setId(String identificacion) {
		this.id = identificacion;
	}
	/**
	 * @return the tipoEmpleado
	 */
	public String getTypeEmployee() {
		return typeEmployee;
	}
	/**
	 * @param tipoEmpleado the tipoEmpleado to set
	 */
	public void setTypeEmployee(String typeEmployee) {
		this.typeEmployee = typeEmployee;
	}
	/**
	 * @return the estado
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * @param estado the estado to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}	
	/**
	 * @return the prioridad
	 */
	public int getPriority() {
		return priority;
	}
	/**
	 * @param prioridad the prioridad to set
	 */
	public void setPriority(int priority) {
		this.priority = priority;
	}		
	@Override
	public int hashCode() {
		return id.hashCode(); 
	}	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Employee) {
			Employee emp = (Employee)obj;
			return id.equals(emp.id);
		}
		return false;
	}
	@Override
	public String toString() {
		return String.format(
				"Employee [name=%s, id=%s, typeEmployee=%s, status=%s, priority=%s]", name,
				id, typeEmployee, status, priority);
	}
	
	public int compareTo(Employee o) {
		Integer i = this.priority;
		return i.compareTo(o.getPriority());
	}	
}
