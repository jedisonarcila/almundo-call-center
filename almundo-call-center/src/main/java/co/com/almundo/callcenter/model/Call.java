package co.com.almundo.callcenter.model;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Class to represent the object call, when is asigne a employee to the instance of the
 * object, immediately start the process of call in the method run
 *  
 * @author jarcila
 */
public class Call implements Runnable {
	
	private String name;
	private String id;
	private String phone;
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * @return the phone
	 */
	public String getPhone() {
		return phone;
	}
	/**
	 * @param telefono the telefono to set
	 */
	public void setTelefono(String phone) {
		this.phone = phone;
	}		
	
	@Override
	public String toString() {
		return String.format("Call [name=%s, id=%s, phone=%s]", name,
				id, phone);
	}
	
	private List<Employee> employees;	
	private Employee employee;
	private long time;
	ThreadLocalRandom random;
	
	/**
	 * @param empleados the employees to set
	 */
	public void setEmployees(List<Employee> employees) {
		this.employees = employees;
	}
	/**
	 * @param employee the employee to set
	 */
	public void setEmployee(Employee employee) {
		this.employee = employee;
	}
	
	public void run() {
			this.random = ThreadLocalRandom.current();
			this.time = random.nextInt(5, 11);
			System.out.println("La llamada hecha por " + this.name + " es atendida por " + employee.getName() + " ... el tiempo de atenci�n es: " + time);
			try {
				Thread.sleep((time*1000));
				//Thread.sleep((time*1));
		    } catch (InterruptedException e) {
		        System.out.println("Hemos sido interrumpidos...");
		        return;
		    }
			int index = employees.indexOf(employee);
			employees.get(index).setStatus("disponible");
			System.out.println("Colgado " + employee.getName() + " ...");	
	}	
	
}
