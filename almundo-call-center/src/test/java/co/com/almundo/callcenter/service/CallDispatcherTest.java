package co.com.almundo.callcenter.service;

import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import co.com.almundo.callcenter.ApiApplicationTests;
import co.com.almundo.callcenter.model.Call;
import co.com.almundo.callcenter.utils.Mocks;

public class CallDispatcherTest extends ApiApplicationTests {
	
	@Autowired
	CallService callService;
	
	@Autowired
	Mocks mocks;

	@Test
	public void testAnswerCall() {
		List<Call> calls = mocks.getCalls();		
		boolean status = callService.startCall(calls);
		assertTrue(status);
	}
	
	@Test
	public void testFifoCall() {
		List<Call> calls = mocks.getCalls();
		BlockingQueue<Call> queue = new ArrayBlockingQueue<Call>(10);
		boolean status = callService.fifoCall(calls, queue);
		assertTrue(status);
	}
	
	@Test
	public void testStartDispatcherCall() {
		//List<Employee> list = mocksD.getEmployee();
		List<Call> calls = mocks.getCalls();
		//when(mocksD.getEmployee()).thenReturn(list);
		boolean status = callService.startCall(calls);
		assertTrue(status);
	}

}
