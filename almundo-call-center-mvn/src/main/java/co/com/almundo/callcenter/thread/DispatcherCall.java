package co.com.almundo.callcenter.thread;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CopyOnWriteArrayList;

import co.com.almundo.callcenter.model.Call;
import co.com.almundo.callcenter.model.Employee;

/**
 * Class incharge the distribute the call to a empleyeed available and by priority
 * 
 * @author jarcila
 *
 */
public class DispatcherCall implements Runnable {
	private BlockingQueue<Call> queue;
	private List<Employee> employee;	
	
	public DispatcherCall(BlockingQueue<Call> queue, List<Employee> employee) {
		this.queue = queue;
		//synchronize the attribute to be modify in other thread
		this.employee = new CopyOnWriteArrayList<Employee>(employee); 
		//Order the employees by priority
		Collections.sort(this.employee);		
	}

	
	public void run() {		
		while (true) {
			try {
				//get a register of the fifo
				Call call = queue.take();
				dispatchCall(call);				
			} catch (InterruptedException e) {				
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Method to incharge the distribute the call to a employee depend the 
	 * status
	 * 
	 * @param call
	 * @throws InterruptedException 
	 */
	public synchronized void dispatchCall(Call call) throws InterruptedException {
		for (int i = 0; i < employee.size(); i++) {
			Employee emp = employee.get(i);
			if (emp.getStatus().equalsIgnoreCase("disponible")) {				
				emp.setStatus("ocupado");
				call.setEmployee(emp);
				call.setEmployees(employee);				
				new Thread(call).start();				
				return;
			}
		}		
		System.out.println("Volviendo a encolar llamada " + call);
		queue.put(call);
	}
	
}
