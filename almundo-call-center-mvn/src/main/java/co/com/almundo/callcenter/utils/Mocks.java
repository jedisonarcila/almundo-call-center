package co.com.almundo.callcenter.utils;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import co.com.almundo.callcenter.model.Call;
import co.com.almundo.callcenter.model.Employee;

@PropertySource("classpath:/mocks.properties")
@Component("mocks")
public class Mocks {
	
	@Value("${almundo.calls}")
	String calls;
	
	@Value("${almundo.employees}")
	String employees;

	/**
	 * @return the calls
	 */
	public List<Call> getCalls() {
		Type listType = new TypeToken<ArrayList<Call>>(){}.getType();
		return new Gson().fromJson(calls, listType);
	}

	/**
	 * @return the supervisors
	 */
	public List<Employee> getEmployee() {
		Type listType = new TypeToken<ArrayList<Employee>>(){}.getType();
		return new Gson().fromJson(employees, listType);
	}	
	
}

